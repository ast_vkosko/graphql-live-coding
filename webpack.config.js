var path = require('path');

module.exports = {
    entry: './client/src/main.js',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'client/static'),
        filename: 'app.js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules)|(server)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-transform-runtime']
                    }
                }
            }
        ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map'
};
