import gql from 'graphql-tag';

const fetchQuery = gql`
query GetData($missionId: ID!, $rocketId: ID!, $dragonId: ID!) {
    missions {
        mission_id
        mission_name
    }
    mission(id: $missionId) {
        mission_name
            description
    }

    rockets {
        rocket_id
            rocket_name
    }
    rocket(id: $rocketId) {
        rocket_name
        description
    }

    dragons {
        id
        name
    }
    dragon(id: $dragonId) {
        name
            description
    }
}
`;

const variables = {
    missionId: '9D1B7E0',
    rocketId: 'falcon9',
    dragonId: 'dragon1'
};

export default {
    query: fetchQuery,
    variables: variables
};
