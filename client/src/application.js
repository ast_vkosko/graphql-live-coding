const FETCH_BTN = 'fetch-btn';
const SHOW_BLOCK = 'show-block';
const RESULTS_BLOCK = 'results';
const HIDE_CLS = 'h-hide';

export default class Application {
    constructor(client, query) {
        this.client = client;
        this.query = query;

        this.showBlock = document.getElementById(SHOW_BLOCK);
        this.resultsBlock = document.getElementById(RESULTS_BLOCK);
        this.fetchButton = document.getElementById(FETCH_BTN);
    }

    init() {
        this.fetchButton.addEventListener('click', (e) => this.fetchData(e));
    }

    async fetchData(e) {
        e.preventDefault();

        let response = await this.client.query(this.query);

        if (response.data) {
            this.showBlock.innerHTML = JSON.stringify(response.data, null, 2);
            this.resultsBlock.classList.remove(HIDE_CLS);
        }
    }
}
