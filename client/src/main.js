import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import Application from './application';
import fetchQuery from './query';

const cache = new InMemoryCache();
const link = new HttpLink({ uri: 'http://localhost:4000/graphql' });
const client = new ApolloClient({ cache, link });

(() => {
    const application = new Application(client, fetchQuery);

    application.init();

    console.log('Application has been started!');
})();
