const resolvers = {
    Query: {
        dragon: async (parent, args, { dataSources }) => {
            let result = await dataSources.dragonDataSource.getDragon(args.id);

            return result;
        },
        dragons: async (parent, args, { dataSources }) => {
            let result = await dataSources.dragonDataSource.getDragons();

            return result;
        }
    }
};

export default resolvers;
