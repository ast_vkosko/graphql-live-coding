import { gql } from 'apollo-server-koa';

const typeDefs = [gql`
    type Dragon {
        id: ID!
        name: String
        type: String
        active: Boolean
        crew_capacity: Int
        sidewall_angle_deg: Int
        orbit_duration_yr: Int
        dry_mass_kg: Int
        dry_mass_lb: Int
        first_flight: String
        heat_shield: DragonHeatShield
        thrusters: [DragonThruster]
        launch_payload_mass: Mass
        launch_payload_vol: DragonPayloadVolume
        return_payload_mass: Mass
        return_payload_vol: DragonPayloadVolume
        pressurized_capsule: DragonCapsule
        trunk: DragonTrunk
        height_w_trunk: Dimension
        diameter: Dimension
        wikipedia: String
        description: String
    }

    extend type Query {
        dragon(id: ID!): Dragon!
        dragons: [Dragon]!
    }

    type DragonCapsule {
        payload_volume: DragonPayloadVolume
    }

    type DragonThruster {
        type: String
        amount: Int
        pods: Int
        fuel_1: String
        fuel_2: String
        thrust: Thrust
    }

    type DragonHeatShield {
        material: String
        size_meters: Float
        temp_degrees: Int
        dev_partner: String
    }

    type DragonPayloadVolume {
        cubic_meters: Int
        cubic_feet: Int
    }

    type DragonTrunk {
        trunk_volume: DragonPayloadVolume
        cargo: DragonTrunkCargo
    }

    type DragonTrunkCargo {
        solar_array: Int
        unpressurized_cargo: Boolean
    }
`];

export default typeDefs;
