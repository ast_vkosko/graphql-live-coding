import typeDefs from './typeDefs';
import resolvers from './resolvers';
import dataSource from './source';

export { typeDefs, resolvers, dataSource };
