import dragons from '../../../mocks/dragons.json';

export default class DragonSource {
    constructor() {
        this.dragons = dragons;
        this.dragonsById = dragons.reduce((acc, dragon) => {
            acc[dragon.id] = dragon;

            return acc;
        }, {});
    }

    async getDragon(id) {
        return this.dragonsById[id];
    }

    async getDragons() {
        return this.dragons;
    }
}
