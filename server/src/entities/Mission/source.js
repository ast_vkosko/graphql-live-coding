import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../../config/api.json';

export default class MissionSource extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `${config.spaceX.baseURL}`;
    }

    async getMission(id) {
        return this.get(`/missions/${id}`);
    }

    async getMissions() {
        return this.get('/missions');
    }
}
