import { gql } from 'apollo-server-koa';

const typeDefs = [gql`
    type Mission {
        mission_id: ID!
        mission_name: String!
        manufacturers: [String]!
        payload_ids: [String]!
        wikipedia: String
        website: String
        twitter: String
        description: String
    }

    extend type Query {
        mission(id: ID!): Mission!
        missions: [Mission]!
    }
`];

export default typeDefs;
