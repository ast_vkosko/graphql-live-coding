const resolvers = {
    Query: {
        mission: async (parent, args, { dataSources }) => {
            let result = await dataSources.missionDataSource.getMission(args.id);

            return result;
        },
        missions: async (parent, args, { dataSources }) => {
            let result = await dataSources.missionDataSource.getMissions();

            return result;
        }
    }
};

export default resolvers;
