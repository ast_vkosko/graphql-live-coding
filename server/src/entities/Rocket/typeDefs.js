import { gql } from 'apollo-server-koa';

const typeDefs = [gql`
    type Rocket {
        id: ID!,
        active: Boolean,
        stages: Int,
        boosters: Int,
        cost_per_launch: Int,
        success_rate_pct: Float,
        first_flight: String,
        country: String,
        company: String,
        height: Dimension
        diameter: Dimension
        mass: Mass
        payload_weights: [PayloadWeight]
        first_stage: FirstStage
        second_stage: SecondStage
        engines: Engines
        landing_legs: LandingLegs
        wikipedia: String
        description: String
        rocket_id: String,
        rocket_name: String
        rocket_type: String
    }

    extend type Query {
        rocket(id: ID!): Rocket!
        rockets: [Rocket]!
    }

    type CompositeFairing {
        height: Dimension
        diameter: Dimension
    }

    type Payloads {
        option_1: String
        option_2: String
        composite_fairing: CompositeFairing
    }

    type PayloadWeight {
        id: ID!
        name: String
        kg: Int
        lb: Int
    }

    type Dimension {
        meters: Float!
        feet: Float!
    }

    type Mass {
        kg: Int
        lb: Int
    }

    type Thrust {
        kN: Int
        lbf: Int
    }

    type FirstStage {
        reusable: Boolean
        engines: Int
        fuel_amount_tons: Int
        burn_time_sec: Int
        thrust_sea_level: Thrust
        thrust_vacuum: Thrust
    }

    type SecondStage {
        engines: Int
        fuel_amount_tons: Int
        burn_time_sec: Int
        thrust: Thrust
        payloads: Payloads
    }

    type Engines {
        number: Int
        type: String
        version: String
        layout: String
        engine_loss_max: Int
        propellant_1: String
        propellant_2: String
        thrust_sea_level: Thrust
        thrust_vacuum: Thrust
        thrust_to_weight: Float
    }

    type LandingLegs {
        number: Int
        material: String
    }
`];

export default typeDefs;
