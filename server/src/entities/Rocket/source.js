import { RESTDataSource } from 'apollo-datasource-rest';
import config from '../../../config/api.json';

export default class RocketSource extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = `${config.spaceX.baseURL}`;
    }

    async getRocket(id) {
        return this.get(`/rockets/${id}`);
    }

    async getRockets() {
        return this.get('/rockets');
    }
}
