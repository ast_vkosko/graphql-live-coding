const resolvers = {
    Query: {
        rocket: async (parent, args, { dataSources }) => {
            let result = await dataSources.rocketDataSource.getRocket(args.id);

            return result;
        },
        rockets: async (parent, args, { dataSources }) => {
            let result = await dataSources.rocketDataSource.getRockets();

            return result;
        }
    }
};

export default resolvers;
