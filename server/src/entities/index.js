import DataSource from '../models/dataSource';
import Entity from '../models/entity';

import { typeDefs as DragonTypeDefs, resolvers as DragonResolvers, dataSource as DragonDataSource } from './Dragon';
import { typeDefs as MissionTypeDefs, resolvers as MissionResolvers, dataSource as MissionDataSource } from './Mission';
import { typeDefs as RocketTypeDefs, resolvers as RocketResolvers, dataSource as RocketDataSource } from './Rocket';

export default [
    new Entity(DragonTypeDefs, DragonResolvers, new DataSource('dragonDataSource', DragonDataSource)),
    new Entity(MissionTypeDefs, MissionResolvers, new DataSource('missionDataSource', MissionDataSource)),
    new Entity(RocketTypeDefs, RocketResolvers, new DataSource('rocketDataSource', RocketDataSource))
];
