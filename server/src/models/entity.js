export default class Entity {
    constructor (typeDefs, resolvers, dataSource) {
        this.typeDefs = typeDefs;
        this.resolvers = resolvers;
        this.dataSource = dataSource || null;
    }
}
