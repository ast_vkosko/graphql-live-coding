import { makeExecutableSchema } from 'graphql-tools';
import { gql } from 'apollo-server-koa';

export default class GraphQLCore {
    constructor() {
        this.typeDefs = [gql`
            type Query {
                _empty: String
            }
            type Mutation {
                _empty: String
            }
        `];
        this.resolvers = {};
        this._dataSources = {};
    }

    get config() {
        return {
            schema: this.executableSchema,
            dataSources: () => this.dataSources
        };
    }

    /**
     * @returns {GraphQLSchema}
     */
    get executableSchema() {
        const schema = makeExecutableSchema({
            typeDefs: this.typeDefs,
            resolvers: this.resolvers
        });

        return schema;
    }

    get dataSources () {
        return this._dataSources;
    }

    /**
     * @param {Entity} entity
     * @returns {GraphQLCore}
     */
    addEntity(entity) {
        this.typeDefs = [...this.typeDefs, ...entity.typeDefs];

        Object.keys(entity.resolvers).forEach(key => {
            if (this.resolvers[key]) {
                this.resolvers[key] = { ...this.resolvers[key], ...entity.resolvers[key] };
            } else {
                this.resolvers[key] = entity.resolvers[key];
            }
        });

        if (entity.dataSource) {
            this._dataSources[entity.dataSource.name] = new entity.dataSource.Source();
        }
    }

    /**
     * @param {Array<Entity>} entities
     * @returns {GraphQLCore}
     */
    addEntities(entities) {
        entities.forEach(entity => {
            this.addEntity(entity);
        });

        return this;
    }
}
