import Koa from 'koa';
import { ApolloServer } from 'apollo-server-koa';
import serve from 'koa-static';
import views from 'koa-views';
import path from 'path';
import GraphQLCore from './graphql';
import entities from './entities';

const graphQLCore = new GraphQLCore();
const graphQLConfig = graphQLCore.addEntities(entities).config;
const server = new ApolloServer(graphQLConfig);
const app = new Koa();

app.use(server.getMiddleware());
app.use(views(path.join(__dirname, '/../views'), { extension: 'ejs' }));
app.use(serve(path.join(__dirname, '/../../client/static')));

app.use(async (ctx) => {
    await ctx.render('home');
});

app.listen({ port: 4000 }, () => {
    console.log( //eslint-disable-line
        `🚀 Server ready at http://localhost:4000${server.graphqlPath}`
    );
});
